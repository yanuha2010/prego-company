 var app = angular.module('myApp', ['ngGrid', 'ngDialog', 'ngFileUpload', 'ui.bootstrap'])

 .factory('GridService1', ['$http', '$q',
     function($http, $q) {
         var contributorsFile = 'json/new-payout-1.json';
         var contributors = [];

         function getContributors() {
             var deferred = $q.defer();
             $http.get(contributorsFile)
                 .then(function(result) {
                     contributors = result.data;
                     deferred.resolve(contributors);
                 }, function(error) {
                     deferred.reject(error);
                 });
             return deferred.promise;
         }
         return {
             getContributors: getContributors
         };
     }
 ])

 app.controller('MyCtrl3', ['$scope', 'GridService1', 'ngDialog', 'Upload',
     function($scope, GridService1, ngDialog, Upload) {
         GridService1.getContributors().then(function(data) {
             $scope.myData = data;
         });

         $scope.gridOptions1 = {
             data: 'myData',
             jqueryUITheme: true,
             enablePinning: false,
             enablePaging: true,
             showFooter: true,
             headerRowHeight: 40,
             totalServerItems: 'totalServerItems',
             pagingOptions: {
                 pageSizes: [15, 30, 60],
                 pageSize: 15,
                 totalServerItems: 0,
                 currentPage: 1
             },
             //filterOptions: $scope.filterOptions,
             columnDefs: [{
                 field: ' ',
                 width: 40,
                 headerClass: 'ng-user-ch__header',
                 cellClass: 'ng-user-ch__wr',
                 cellTemplate: '<div class="ngSelectionCell"><input tabindex="-1" class="ngSelectionCheckbox" type="checkbox"/></div>'
             }, {
                 field: 'Name',
                 displayName: 'Name'
             }, {
                 field: 'ID_Number',
                 displayName: 'ID Number'
             }, {
                 field: ' ',
                 width: 40,
                 headerClass: 'ng-user-add__header',
                 cellClass: 'ng-user-add__wr',
                 cellTemplate: '<div class="ngCellText"><span class="grid-plus-btn"><i class="icon i-plus"></i></span></div>'
             }],
             headerRowTemplate: '../headerRowTemplate.html',
             showFooter: false
                 // showSelectionCheckbox: true,
                 // checkboxCellTemplate: '<div class="ngSelectionCell"><input tabindex="-1" class="ngSelectionCheckbox" type="checkbox" ng-checked="row.selected" /> Some text here</div>',

         };

         $scope.gridOptions2 = {
             data: 'myData',
             jqueryUITheme: true,
             enablePinning: false,
             enablePaging: true,
             showFooter: true,
             headerRowHeight: 40,
             totalServerItems: 'totalServerItems',
             pagingOptions: {
                 pageSizes: [15, 30, 60],
                 pageSize: 15,
                 totalServerItems: 0,
                 currentPage: 1
             },
             //filterOptions: $scope.filterOptions,
             columnDefs: [{
                 field: ' ',
                 width: 40,
                 headerClass: 'ng-user-ch__header',
                 cellClass: 'ng-user-ch__wr',
                 cellTemplate: '<div class="ngSelectionCell"><input tabindex="-1" class="ngSelectionCheckbox" type="checkbox"/></div>'
             }, {
                 field: 'Name',
                 displayName: 'Name'
             }, {
                 field: 'Amount'
             }, {
                 field: 'Edit',
                 width: 60,
                 headerClass: 'ng-user-add__header',
                 cellClass: 'ng-user-add__wr',
                 cellTemplate: '<div class="ngCellText"><span class="grid-del-btn"><i class="icon i-trash"></i></span></div>'
             }],
             headerRowTemplate: '../headerRowTemplate.html',
             showFooter: false
                 // showSelectionCheckbox: true,
                 // checkboxCellTemplate: '<div class="ngSelectionCell"><input tabindex="-1" class="ngSelectionCheckbox" type="checkbox" ng-checked="row.selected" /> Some text here</div>',
         };

         $scope.clickUploadPayout = function() {
             ngDialog.open({
                 template: 'upload-payout-modal.html',
                 className: 'ngdialog-theme-default ng-modal-lg',
                 scope: $scope
             });
         };

         $scope.clickImportCompleted = function() {
             ngDialog.open({
                 template: 'upload-import-completed-modal.html',
                 className: 'ngdialog-theme-default ng-modal-md',
                 scope: $scope
             });
         };

         $scope.$watch('files', function() {
             $scope.upload($scope.files);
         });
         $scope.log = '';

         $scope.upload = function(files) {
             if (files && files.length) {
                 for (var i = 0; i < files.length; i++) {
                     var file = files[i];
                     $scope.file_ = file;
                     Upload.upload({
                         url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                         file: file
                     }).progress(function(evt) {
                         var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                         $scope.log = 'progress: ' + progressPercentage + '% ' +
                             evt.config.file.name + '\n' + $scope.log;
                         $scope.progress = progressPercentage + '%';
                     }).success(function(data, status, headers, config) {
                         $scope.log = 'file ' + config.file.name + 'uploaded. Response: ' + JSON.stringify(data) + '\n' + $scope.log;
                         $scope.$apply();
                     });
                 }
             }
         };

         $scope.today = function() {
             $scope.dt = new Date();
         };
         $scope.today();

         $scope.clear = function() {
             $scope.dt = null;
         };

         // Disable weekend selection
         $scope.disabled = function(date, mode) {
             return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
         };

         $scope.toggleMin = function() {
             $scope.minDate = $scope.minDate ? null : new Date();
         };
         $scope.toggleMin();

         $scope.open = function($event) {
             $event.preventDefault();
             $event.stopPropagation();

             $scope.opened = true;
         };

         $scope.dateOptions = {
             formatYear: 'yy',
             startingDay: 1,
             showWeeks:'false'
         };

         $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
         $scope.format = $scope.formats[0];

         var tomorrow = new Date();
         tomorrow.setDate(tomorrow.getDate() + 1);
         var afterTomorrow = new Date();
         afterTomorrow.setDate(tomorrow.getDate() + 2);
         $scope.events = [{
             date: tomorrow,
             status: 'full'
         }, {
             date: afterTomorrow,
             status: 'partially'
         }];

         $scope.showWeeks = false;

         $scope.getDayClass = function(date, mode) {
             if (mode === 'day') {
                 var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                 for (var i = 0; i < $scope.events.length; i++) {
                     var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                     if (dayToCheck === currentDay) {
                         return $scope.events[i].status;
                     }
                 }
             }

             return '';
         };

     }

 ]);
