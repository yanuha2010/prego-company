 var app = angular.module('myApp', ['ngGrid', 'ngDialog'])

 .factory('GridService2', ['$http', '$q',
     function($http, $q) {
         var contributorsFile = 'json/payout.json';
         var contributors = [];
         function getContributors() {
             var deferred = $q.defer();
             $http.get(contributorsFile)
                 .then(function(result) {
                     contributors = result.data;
                     deferred.resolve(contributors);
                 }, function(error) {
                     deferred.reject(error);
                 });
             return deferred.promise;
         }
         return {
             getContributors: getContributors
         };
     }
 ]);

app.controller('MyCtrl2', ['$scope', 'GridService2', 'ngDialog',
     function($scope, GridService2, ngDialog) {
         GridService2.getContributors().then(function(data) {
             $scope.myData = data;
         });

         $scope.gridOptions = {
             data: 'myData',
             jqueryUITheme: true,
             enablePinning: false,
             enablePaging: true,
             showFooter: true,
             headerRowHeight: 40,
             totalServerItems: 'totalServerItems',
             pagingOptions: {
                 pageSizes: [15, 30, 60],
                 pageSize: 15,
                 totalServerItems: 0,
                 currentPage: 1
             },
             //filterOptions: $scope.filterOptions,
             columnDefs: [
                 {field:'Data', displayName: 'Data'},
                 {field:'Name', displayName:'Name'},
                 {field:'Reference'},
                 {field:'Data_2', displayName:'Date'},
                 {field:'Mempers'},
                 {field:'Amount'},
                 {field:'Stauts'}
             ],
             headerRowTemplate: '../headerRowTemplate.html',
             footerTemplate: '../footerTemplate.html'
         };


        $scope.clickToOpen = function () {
            ngDialog.open({ 
                template: 'new-payout-modal.html',
                className: 'ngdialog-theme-default ng-modal-lg'
             });
        };
     }

]);

//  (function () {
//   var myApp = angular.module('myApp', ['angular-bootstrap-select']);

//   myApp.controller('MyCtrl2', function ($scope, $timeout) {
//     $scope.model = '';
//     $scope.colors = ['Mustard', 'Ketchup', 'Relish'];
//     $scope.repeater = [
//       { title: 'one' },
//       { title: 'two' },
//       { title: 'three' }
//     ];
//     $scope.selectWithOptionsIsVisible = true;
//   });
  
  
//   /**
//    * inject angular-bootstrap-select
//    * https://raw.githubusercontent.com/joaoneto/angular-bootstrap-select/v0.0.4/src/angular-bootstrap-select.js
//    */

//   angular.module('angular-bootstrap-select', [])
//   .directive('selectpicker', ['$parse', function ($parse) {
//     return {
//       restrict: 'A',
//       link: function (scope, element, attrs) {
//         element.selectpicker($parse(attrs.selectpicker)());
//         element.selectpicker('refresh');
        
//         scope.$watch(attrs.ngModel, function (newVal, oldVal) {
//           scope.$parent[attrs.ngModel] = newVal;
//           scope.$evalAsync(function () {
//             if (!attrs.ngOptions || /track by/.test(attrs.ngOptions)) element.val(newVal);
//             element.selectpicker('refresh');
//           });
//         });
        
//         scope.$on('$destroy', function () {
//           scope.$evalAsync(function () {
//             element.selectpicker('destroy');
//           });
//         });
//       }
//     };
//   }]);
// })();


