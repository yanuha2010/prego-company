 var app = angular.module('myApp', ['ngGrid', 'ngTagsInput'])

   .factory('GridService', ['$http', '$q',
       function($http, $q) {
           var contributorsFile = 'json/members.json';
           var contributors = [];
           function getContributors() {
               var deferred = $q.defer();
               $http.get(contributorsFile)
                   .then(function(result) {
                       contributors = result.data;
                       deferred.resolve(contributors);
                   }, function(error) {
                       deferred.reject(error);
                   });
               return deferred.promise;
           }
           return {
               getContributors: getContributors
           };
       }
   ])

   .controller('MyCtrl', ['$scope', 'GridService',
       function($scope, gridService) {
           gridService.getContributors().then(function(data) {
               $scope.myData = data;
           });

           $scope.gridOptions = {
                data: 'myData',
                jqueryUITheme: true,
                enablePinning: false,
                enablePaging: true,
                showFooter: true,
                headerRowHeight: 40,
                enableCellEditOnFocus: true,
                totalServerItems: 'totalServerItems',
                pagingOptions: {
                   pageSizes: [15, 30, 60],
                   pageSize: 15,
                   totalServerItems: 0,
                   currentPage: 1
               },
               //filterOptions: $scope.filterOptions,
               columnDefs: [
                  {
                    field:'User_status',
                    displayName: ' ',
                    headerClass: 'ng-user-status__header',
                    cellClass: 'ng-user-status__wr',
                    cellTemplate: '<div class="ngCellText text-left"><span class="user-status {{row.entity[col.field]}}"></span><span class="user-status__label">Join Request</span></div>',
                    enableCellEdit: true,
                    cellClass: 'ng-form-control__edit-wr',
                    editableCellTemplate: '<div class="input-group"><select class="form-control" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-options="id as name for (id, name) in statuses"/><div class="input-group-btn"><button type="button" class="btn btn-del"><i class="icon i-error"></i></button><button type="button" class="btn btn-save"><i class="icon i-success"></i></button></div></div>'
                  },
                  {
                    field:'ID_Namber',
                    displayName:'ID Namber',
                    enableCellEdit: true,
                    cellClass: 'ng-form-control__edit-wr',
                    editableCellTemplate: '<div class="input-group"><input class="form-control" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD"/><div class="input-group-btn"><button type="button" class="btn btn-del"><i class="icon i-error"></i></button><button type="button" class="btn btn-save"><i class="icon i-success"></i></button></div></div>'
                  },
                  {
                    field:'Name',
                    displayName:'Name'
                  },
                  {field:'Date_of_Birth', displayName:'Date of Birth'},
                  {field:'Country'},
                  {field:'Email'},
                  {field:'Contact_No', displayName:'Contact No.'}
               ],
               headerRowTemplate: '../headerRowTemplate.html',
               footerTemplate: '../footerTemplate.html'
           };

           $scope.gridOptions4 = {
             data: 'myData',
             jqueryUITheme: true,
             enablePinning: false,
             enablePaging: true,
             showFooter: true,
             headerRowHeight: 40,
             totalServerItems: 'totalServerItems',
             pagingOptions: {
                 pageSizes: [15, 30, 60],
                 pageSize: 15,
                 totalServerItems: 0,
                 currentPage: 1
             },
             //filterOptions: $scope.filterOptions,
             columnDefs: [{
                 field: 'Date',
                 width: 200,
             }, {
                 field: 'Reference'
             }, {
                 field: 'Amount',
                 width: 150,
             }],
             headerRowTemplate: '../headerRowTemplate.html',
             showFooter: false
                 // showSelectionCheckbox: true,
                 // checkboxCellTemplate: '<div class="ngSelectionCell"><input tabindex="-1" class="ngSelectionCheckbox" type="checkbox" ng-checked="row.selected" /> Some text here</div>',
         };
       }

   ]);